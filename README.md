# Java-G Compiler #

Java-G is a programming language specifically designed for code golfers. Using a Java base, users can use aliases and shortcuts to do many things that are long in Java.
Visit the wiki for more information!