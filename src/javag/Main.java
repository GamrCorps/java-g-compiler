package src.javag;

import java.io.File;
import java.io.FileNotFoundException;

import static java.lang.System.out;

class Main {

    public static void main(String[] args) throws FileNotFoundException {
        if (args.length > 0) {
            File compilerFile = new File(args[0]);
            if (compilerFile.exists()) {
                for (RegexEnum regex : RegexEnum.values()) {
                    regex.run(compilerFile, compilerFile.toString().indexOf(regex.getRegex()));
                }
            } else {
                throw new FileNotFoundException();
            }
        } else {
            out.println("Missing parameters! Needed parameters: <fileToCompile>");
        }
    }
}
