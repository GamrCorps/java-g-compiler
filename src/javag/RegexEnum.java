package src.javag;

import src.javag.Runnables.Class.ClassRunnable;
import src.javag.Runnables.Class.PublicClassRunnable;
import src.javag.Runnables.Importing.ImportJavaRunnable;
import src.javag.Runnables.Importing.ImportRunnable;
import src.javag.Runnables.Importing.ImportStaticJavaRunnable;
import src.javag.Runnables.Importing.ImportStaticRunnable;
import src.javag.Runnables.Method.MainMethodRunnable;
import src.javag.Runnables.RegexRunnable;
import src.javag.Runnables.STDOUT.PrintFormatRunnable;
import src.javag.Runnables.STDOUT.PrintLineRunnable;
import src.javag.Runnables.STDOUT.PrintRunnable;
import src.javag.Runnables.System.SystemExitRunnable;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public enum RegexEnum {
    IMPORT("_i_", "import ", new ImportRunnable()),
    IMPORT_JAVA("_ij_", "import java.", new ImportJavaRunnable()),
    IMPORT_STATIC("_is_", "import static", new ImportStaticRunnable()),
    IMPORT_STATIC_JAVA("_isj_", "import static java.", new ImportStaticJavaRunnable()),
    PUBLIC_CLASS("pcl_", "public class ", new PublicClassRunnable()),
    CLASS("cl_", "class", new ClassRunnable()),
    MAIN_METHOD("main\\{", "public static main (String[] args) {", new MainMethodRunnable()),
    PRINT("print\\(", "System.out.print(", new PrintRunnable()),
    PRINT_LINE("printl\\(", "System.out.println(", new PrintLineRunnable()),
    PRINT_FORMAT("printf\\(", "System.out.printf(", new PrintFormatRunnable()),
    SYSTEM_EXIT("exit\\(","System.exit(",new SystemExitRunnable());

    private final String regex;
    private final String replace;
    private final RegexRunnable[] runnables;

    RegexEnum(String stringToLookFor, String replacingString, RegexRunnable... actions) {
        regex = stringToLookFor;
        runnables = actions;
        replace = replacingString;
    }

    public String getRegex() {
        return regex;
    }

    public RegexRunnable[] getRunnables() {
        return runnables;
    }

    public Object[] run(File file, int index) {
        List<Object> returns = new ArrayList<Object>();
        for (RegexRunnable run : runnables) {
            returns.add(run.run(file, index));
        }
        return returns.toArray();
    }

    public String getReplace() {
        return replace;
    }
}
