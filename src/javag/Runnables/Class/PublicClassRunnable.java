package src.javag.Runnables.Class;

import src.javag.FileFunctions;
import src.javag.RegexEnum;
import src.javag.Runnables.RegexRunnable;

import java.io.File;

/**
 * Created by matthewmccaskill on 2/18/15.
 */
public class PublicClassRunnable extends RegexRunnable<Integer> {

    @Override
    public Integer run(File file, int index) {
        fileIndex = index;
        compilerFile = file;

        FileFunctions.findAndReplaceAll(compilerFile, RegexEnum.PUBLIC_CLASS.getRegex(), RegexEnum.PUBLIC_CLASS.getReplace(), getFoundMessage());

        return 0;
    }

    @Override
    public File getFile() {
        return compilerFile;
    }

    @Override
    public int getIndex() {
        return fileIndex;
    }

    @Override
    public String getName() {
        return "PUBLIC_CLASS";
    }
}
