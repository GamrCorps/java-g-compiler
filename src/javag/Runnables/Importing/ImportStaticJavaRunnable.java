package src.javag.Runnables.Importing;

import src.javag.FileFunctions;
import src.javag.RegexEnum;
import src.javag.Runnables.RegexRunnable;

import java.io.File;

/**
 * Created by Matthew on 2/17/2015.
 */
public class ImportStaticJavaRunnable extends RegexRunnable<Integer> {
    private File compilerFile;
    private int fileIndex;

    @Override
    public Integer run(File file, int index) {
        compilerFile = file;
        fileIndex = index;

        FileFunctions.findAndReplaceAll(compilerFile, RegexEnum.IMPORT_STATIC_JAVA.getRegex(), RegexEnum.IMPORT_STATIC_JAVA.getReplace(), getFoundMessage());

        return 0;
    }

    @Override
    public File getFile() {
        return compilerFile;
    }

    @Override
    public int getIndex() {
        return fileIndex;
    }

    @Override
    public String getName() {
        return "IMPORT_STATIC_JAVA";
    }
}
