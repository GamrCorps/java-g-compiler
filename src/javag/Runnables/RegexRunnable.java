package src.javag.Runnables;

import java.io.File;

public abstract class RegexRunnable<Type>{

    protected File compilerFile;
    protected int fileIndex;

    public abstract Type run(File file, int index);

    public abstract File getFile();

    public abstract int getIndex();

    protected String getFoundMessage() {
        return "Found %s occurrence(s) of " + getName() + " on line %s.";
    }

    protected abstract String getName();
}
