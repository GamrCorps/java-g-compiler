package src.javag.Runnables.STDOUT;

import src.javag.FileFunctions;
import src.javag.RegexEnum;
import src.javag.Runnables.RegexRunnable;

import java.io.File;

/**
 * Created by matthewmccaskill on 2/18/15.
 */
public class PrintRunnable extends RegexRunnable<Integer> {
    @Override
    public Integer run(File file, int index) {
        compilerFile = file;
        fileIndex = index;

        FileFunctions.findAndReplaceAll(compilerFile, RegexEnum.PRINT.getRegex(), RegexEnum.PRINT.getReplace(), getFoundMessage());

        return 0;
    }

    @Override
    public File getFile() {
        return compilerFile;
    }

    @Override
    public int getIndex() {
        return fileIndex;
    }

    @Override
    public String getName() {
        return "PRINT";
    }
}
